#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A node class script to create a neural network
"""


from random import random
from .thresholds import calculate


class Node():
    """
    A class which represents a node of a neural network

    ...

    Attributes
    ----------
    nodes : dic
        a list that contains each node from layers of a neural network
    bias : float
        a bias value for calculating result
    weights : dic
        a dictionary that contains weights for previous layer's nodes
    input : float
        input value for calculating
    output : float
        a value after calculating threshold
    nn : testi.neuralNet.NeuralNet
        a testi.neuralNet.NeuralNet object which a node belongs to
    thres : list
        a list of arguments for a treshold function
    layer : int
        layer number of node

    Methods
    -------
    addNode()
        Adds node to Node.nodes
    checkNeuralNet()
        Creates a dictionary for a neural network if it isn't already created
    checkLayer()
        Creates a list for a layer if it isn't already created
    inNode(inputValue)
        Sets and calculates input and output value
    outNode()
        Sets and calculates output value
    updateNode(diff)
        Updates weights of a node
    weightenNode()
        Sets weights of a node
    weightenNodes(nn)
        Sets weights of all nodes in a neural network
    """

    nodes = {}

    def __init__(self, neuralNet, threshold, layer):
        """
        Parameters
        ----------
        neuralNet : testi.neuralNet.NeuralNet
            a testi.neuralNet.NeuralNet object which a node belongs to
        thres : list
            a list of arguments for a treshold function
        layer : int
            layer number of node
        """
        self.bias = round(random(), 2)
        self.weights = {}
        self.input = 0
        self.output = 0
        self.nn = neuralNet
        self.thres = threshold
        self.layer = layer
        self.addNode()

    def addNode(self):
        """Adds node to Node.nodes"""
        self.checkNeuralNet()
        self.checkLayer()
        self.nodes[self.nn][self.layer].append(self)

    def checkNeuralNet(self):
        """Creates a dictionary for a neural network if it isn't already
        created
        """
        try:
            self.nodes[self.nn]
        except Exception:
            self.nodes[self.nn] = {}

    def checkLayer(self):
        """Creates a list for a layer if it isn't already created"""
        try:
            self.nodes[self.nn][self.layer]
        except Exception:
            self.nodes[self.nn][self.layer] = []

    def inNode(self, inputValue):
        """Sets and calculates input and output value

        Parameters
        ----------
        inputValue : float
            An input value for calculate
        """
        self.input = inputValue
        self.output = calculate([self.input]+self.thres)

    def outNode(self):
        """Sets and calculates output value"""
        node = self.nodes[self.nn][self.layer-1]
        plus = 0
        for i in node:
            plus += i.output*self.weights[i]
        plus += self.bias
        self.output = calculate([plus]+self.thres)

    def updateNode(self, diff):
        """Updates weights of a node

        Parameters
        ----------
        diff : float
            Change value for update
        """
        for node in self.nodes[self.nn][self.layer-1]:
            self.weights[node] += diff

    def weightenNode(self):
        """Sets weights of a node"""
        for node in self.nodes[self.nn][self.layer-1]:
            self.weights[node] = round(random(), 2)

    def weightenNodes(nn):
        """Sets weights of all nodes in a neural network

        Parameters
        ----------
        nn : testi.neuralNet.NeuralNet
            A neural network to give weights
        """
        nodes = []
        for node in Node.nodes[nn].values():
            nodes += node
        nodes = nodes[nn.nodeMap[0]:]
        for node in nodes:
            node.weightenNode()
