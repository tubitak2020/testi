#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Threshold functions used in Node
"""


from math import exp, sin, radians


def sigmoid(x, k, a, l):
    """A logistic function

    Parameters
    ----------
    x : float
        The value that will calculated
    k : int
        Steepness of the curve
    a : int
        x value of Sigmoid's midpoint
    l : int
        Maximum value

    Returns
    -------
    float
        Result of function
    """
    return l/(1+exp(-k(x-a)))


def sinus(x, m, n):
    """A sinus function

    Parameters
    ----------
    x : float
        The value that will calculated
    m : int
        Max an min value
    n : int
        Shifting amount

    Returns
    -------
    float
        Result of function
    """
    if m == 0:
        print("Attention! Threshold function isn't a sinus function.")
    return n+(m*sin(radians(x)))


"A dictionary to acces fuctions easier"
functions = {"sigmoid": lambda arg: sigmoid(arg[0], arg[1], arg[2], arg[3]),
             "sin": lambda arg: sinus(arg[0], arg[1], arg[2])}


def calculate(arg):
    """A simple function to calculate

    Parameters
    ----------
    arg : list
        List of specificators and a value to calculate

    Returns
    -------
    float
        A float value after calculate
    """
    return functions[arg[-1]](arg)
