#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
A neural network class script that is built with Node objects
"""


from .node import Node


class NeuralNet():
    """
    A class which represents a neural network that made from Node objects

    ...

    Attributes
    ----------
    neuralNets : list
        a list that contains each NeuralNet object created
    threshold : list
        a list of arguments for a treshold function
    nodeMap : list
        a list that shows how many nodes have each layer
    layer : int
        number of layers
    nodes : dic
        a dictionary that contains list of nodes in a layer with layer key
    nodeList : list
        a list that contains nodes of neural network

    Methods
    -------
    add()
        Appends a neural network to neuralNets
    createNodes(nodes)
        Creates Node objects which are representing nodes of a neural network
    setNodeList()
        Sets nodeList for neural network
    result(inputList)
        Returns a list that contains results of the neural network
    findDiff(inputList, outputList)
        Returns a list of difference between outputs and inputs of a neural
        network object
    update(diffList, a)
        Updates weights of a neural network
    train(inputList, outputList, a=0.5):
        Updates weights of a neural network
    weights()
        Returns string of weights of nodes
    """

    neuralNets = []

    def __init__(self, nodes, thres):
        """
        Parameters
        ----------
        nodes : list
            A list that shows how many nodes have each layer
        thres : list
            Specificator of threshold function
            Logistic Function : [k, a, l, "sigmoid"]
                k : int
                    Steepness of the curve
                a : int
                    x value of Sigmoid's midpoint
                l : int
                    Maximum value
            Sinus Function : [m, n, "sin"]
                m : int
                    Max an min value
                n : int
                    Shifting amount
        """
        self.threshold = thres
        self.nodeMap = nodes
        self.layer = len(nodes)
        self.createNodes(nodes)
        self.setNodeList()
        NeuralNet.add(self)

    def add(self):
        """Appends a neural network to neuralNets"""
        NeuralNet.neuralNets.append(self)

    def createNodes(self, nodes):
        """Creates Node objects which are representing nodes of a neural
        network and sets nodes dictionry of neural newtwork

        Parameters
        ----------
        nodes : list
            A list that shows how many nodes have each layer
        """
        for node in range(len(nodes)):
            for i in range(nodes[node]):
                Node(self, self.threshold, node)
        Node.weightenNodes(self)
        self.nodes = Node.nodes[self]

    def setNodeList(self):
        """Sets nodeList for neural network"""
        nodes = []
        for node in self.nodes.values():
            nodes += node
        self.nodeList = nodes

    def result(self, inputList):
        """Returns a list that contains results of the neural network

        Parameters
        ----------
        inputList : list
            A list that contains input values for each input node

        Returns
        -------
        list
            A list of integers of each output from a neural network

        Example
        -------
            >>> import testi
            >>> nn = testi.NeuralNet([3,4,1,2], [1,0,"sin"])
            >>> nn.result([1,2,3])
            [0.08246033548969676, 0.07758909147106598]
        """
        for i in range(len(inputList)):
            self.nodes[0][i].inNode(inputList[i])
        for i in range(1, self.layer):
            nodes = self.nodes[i]
            for node in nodes:
                node.outNode()
        output = []
        for out in self.nodes[self.layer-1]:
            output.append(out.output)
        return output

    def findDiff(self, inputList, outputList):
        """Returns a list of difference between outputs and inputs of a neural
        network object

        Parameters
        ----------
        inputList: list
            A list that contains values each for a input node
        outputList: list
            A list that contains values each for a output node

        Returns
        -------
        list
            A list that contains differences each for a output node
        """
        resultList = self.result(inputList)
        diff = []
        for out in outputList:
            diff.append(out-resultList[outputList.index(out)])
        return diff

    def update(self, diffList, a):
        """Updates weights of a neural network

        Parameters
        ----------
        diffList : list
            A list that contains change values each for output
        a : int
            Alpha value for set changing value
        """
        for i in range(len(diffList)):
            diff = diffList[i]*a
            for node in self.nodeList[self.nodeMap[0]:]:
                node.updateNode(diff)

    def train(self, inputList, outputList, a=0.01):
        """Updates weights of a neural network

        Parameters
        ----------
        inputList : list
            Contains input values each for a input node
        outputList : list
            Contains wanted return values
        a : int, optonal
            Alpha value for changing train velocity (default is 0.01)

        Example
        -------
            >>> import testi
            >>> nn = testi.NeuralNet([2, 1, 3, 1], [1, 0, "sin"])
            >>> nn.result([1, 2])
            [0.015716149587428216]
            >>> nn.train([1, 2], [1])
            >>> nn.result([1, 2])
            [0.015999315523720536]
        """
        diff = self.findDiff(inputList, outputList)
        self.update(diff, a)

    def weights(self):
        """Returns string of weights of nodes

        Returns
        -------
        str
            An organized string that contains weights of nodes

        Example
        -------
            >>> import testi
            >>> nn = testi.NeuralNet([2, 1], [1, 0, "sin"])
            >>> print(nn.weights())
            0:
                0:
                    input: 0
                1:
                    input: 0
            1:
                0:
                    0:0: 0.18
                    0:1: 0.76
        """
        text = ""
        for layer in self.nodes.keys():
            text += f"{layer}:"
            nodes = self.nodes[layer]
            for node in nodes:
                text += f"\n\t{nodes.index(node)}:"
                if layer != 0:
                    i = 0
                    for weight in node.weights.values():
                        text += f"\n\t\t{layer-1}:{i}: {weight}"
                        i += 1
                    text += "\n"
                else:
                    text += f"\n\t\tinput: {node.input}"
            text += "\n"
        return text[:-2]

