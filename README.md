# Testi
A Python mini-module for making AI projects.
## Installation
In your project's folder:
```
git clone https://gitlab.com/berkaygunduz/testi
```
## Example
Creating a neural ntwork:
```python3
>>> import testi
>>> nodesPerLayer = [2,5,3]
>>> thresholdParam = [1, 0, "sin"]
>>> nn = testi.NeuralNet(nodesPerLayer, thresholdParam)
>>> nn
<testi.neuralNet.NeuralNet object at 0x7fbe202faa90>
```
Returning results of an array for that neural network:
```python3
>>> nn.result([3,14])
[0.0319038244939775, 0.033718596917485286, 0.031924635798269416]
```
Training with an array for that neural network:
```python3
>>> nn.result([3,14])
[0.0319038244939775, 0.033718596917485286, 0.031924635798269416]
>>> nn.train([3,14], [-1, 0.6, 0.8], 0.5)
>>> nn.result([3,14])
[0.040098523056636096, 0.041847855275275556, 0.04011111295050864]
```
Printing weights for that neural network:
```python3
>>> print(nn.weights())
0:
	0:
		input: 3
	1:
		input: 14
1:
	0:
		0:0: 7.847624541749772
		0:1: 8.507624541749774

	1:
		0:0: 7.787624541749773
		0:1: 8.357624541749773

	2:
		0:0: 8.197624541749773
		0:1: 8.157624541749772

	3:
		0:0: 7.887624541749773
		0:1: 7.697624541749773

	4:
		0:0: 8.177624541749774
		0:1: 7.687624541749772

2:
	0:
		1:0: 8.377624541749773
		1:1: 7.697624541749773
		1:2: 7.897624541749773
		1:3: 7.917624541749772
		1:4: 7.727624541749773

	1:
		1:0: 7.617624541749773
		1:1: 7.647624541749773
		1:2: 7.977624541749773
		1:3: 7.797624541749773
		1:4: 7.947624541749773

	2:
		1:0: 7.777624541749773
		1:1: 8.077624541749772
		1:2: 7.617624541749773
		1:3: 7.647624541749773
		1:4: 8.417624541749774
```
